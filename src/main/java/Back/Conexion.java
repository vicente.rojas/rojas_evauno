/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Acer
 */
public class Conexion {
private String base="bddgranja";
private String user= "root";
private String password="";
private String url = "jdbc:mysql://localhost:3306/"+base;
private Connection conexion=null;

public Connection getConexion(){
try{
conexion=(Connection)DriverManager.getConnection(this.url, this.user, this.password);
}catch (SQLException e){
    System.out.println(e);
}
return conexion;
}
}
