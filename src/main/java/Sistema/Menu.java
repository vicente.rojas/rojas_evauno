/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Animalitos.Gallina;
import Back.Admin;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Acer
 */
public class Menu {
  static Admin adminpollos=new Admin();
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int opcion;
        int agregar;
        ArrayList<Gallina> lista_gallinas = new ArrayList<Gallina>();
do{
        System.out.println("   - - - - - - - - - - - - - - ");
        System.out.println(" |            MENÚ             |");
        System.out.println(" | - - - - - - - - - - - - - - |");
        System.out.println(" | 1.- Agregar Info. gallina   |");
        System.out.println(" | 2.- Consultar Info. gallina |");
        System.out.println(" | 3.- Eliminar gallina        |");
        System.out.println(" | 0.- Salir                   |");
        System.out.println(" | - - - - - - - - - - - - - - |");
        System.out.println(" |                             |");
        System.out.println(" | Ingrese una opcion:         |");
        opcion = scan.nextInt();

        switch (opcion) {
            case 1:
                do{
                Gallina pollo = new Gallina();
                
                System.out.println("Ingrese el nombre de la gallina");
                String nombre = scan.next();
                
                System.out.println("Ingrese el genero de la gallina");
                String genero = scan.next();
                
                System.out.println("Ingrese el color de la gallina");
                String color = scan.next();
                
                System.out.println("Asignele un digito identificador");
                int id = scan.nextInt();
                
                pollo.setNombre(nombre);
                pollo.setGenero(genero);
                pollo.setColor(color);
                pollo.setId(id);
                boolean resp=adminpollos.agregar(pollo);
                if(resp==true){
                System.out.println("Informacion agregada correctamente");
                }else{
                    System.out.println("Error al agregar datos");
                }
                
                
                System.out.println("¿Desea agregar otra gallina?");
                System.out.println("   1.-Si            2.-No   ");
                agregar=scan.nextInt();
                }while(agregar==1);
                break;

            case 2:
                System.out.println("Ingrese el nombre de la gallina");
                String nombreg=scan.next();
                
                boolean resp_consulta = adminpollos.consultar(nombreg);
                if(resp_consulta==false){
                    System.out.println("Gallina no encontrada");
                
                }
                
                break;
                
            case 3:
                do{
                    boolean resp_eliminar=false;
                    System.out.println("Ingrese el nombre de la gallina que desee eliminar: ");
                    String eliminar_pollo=scan.next();
                    
                    System.out.println("");
                    
                    boolean resp_elimina = adminpollos.consultar(eliminar_pollo);
                    if(resp_elimina==false){
                        System.out.println("Gallina no encontrada");
                    }else{
                        System.out.println("¿Seguro que desea eliminar los datos en pantalla?");
                        System.out.println("       1.-Si                           2.-No     ");
                        int confirmacion=scan.nextInt();
                        if(confirmacion==1){
                        resp_eliminar=adminpollos.eliminar(eliminar_pollo);
                        }
                        if(resp_eliminar==true){
                            System.out.println("Informacion eliminada correctamente");
                            
                        }
                    }
                    
                    
                    
                    
                    System.out.println("¿Eliminar info. de otra gallina?");
                    System.out.println("   1.-Si                2.-No   ");
                   agregar=scan.nextInt();
                }while(agregar==1);
                break;

            default:
                System.out.println("Ingrese una opción válida");
        }
        System.out.println("");
        System.out.println("¿Volver al menú principal?");
        System.out.println("   1.-Si          2.-No   ");
        opcion=scan.nextInt();
    }while(opcion==1);
    }

}
