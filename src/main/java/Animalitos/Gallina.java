/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animalitos;

/**
 *
 * @author Acer
 */
public class Gallina {

    private String nombre;
    private String genero;
    private String color;
    private int id;

//CONSTRUCTOR
    public Gallina() {
    }

    public Gallina(String nombre, String genero, String color, int id) {
        this.nombre = nombre;
        this.genero = genero;
        this.color = color;
        this.id = id;
    }

//GETTER & SETTER
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
